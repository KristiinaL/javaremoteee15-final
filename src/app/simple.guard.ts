import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class SimpleGuard implements CanActivate {
  constructor(private tokeService: TokenService, private router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.controlGuard();
  }
  
  controlGuard(): boolean {
    if (this.tokeService.token === 2) {
      console.log('Token is 1');
      this.router.navigate(['/home']);
    }
    return this.tokeService.token > 0;
  }
}
