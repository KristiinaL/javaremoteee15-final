import { Component, OnInit } from '@angular/core';
import { Country } from '../countries.interface';
import { HttpService } from '../http.service';

@Component({
  selector: 'sda-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  countries: Array<Country> = [];
  constructor(private httpService: HttpService) {}

  ngOnInit(): void {
    this.getCountries();
   }

  removeCountry(id: number): void {
    this.httpService.deleteCountry(id).subscribe(() => {
      this.getCountries();
    });
  }

  getCountries(): void {
    this.httpService.getCountries().subscribe((data: Array<Country>): void => {
      this.countries = data;
    });
  }
}
