import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country } from './countries.interface';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  baseUrl = 'http://localhost:8080';
  constructor(private http: HttpClient) {}

  getMeAPicture(): Observable<any> {
    const url = 'https://cors-anywhere.herokuapp.com/http://shibe.online/api/shibes?count=1&urls=true&httpsUrls=true'
    return this.http.get(url);
  }
// SPRING SERVER DEMO
   getCountries(): Observable<Array<Country>> {
    return this.http.get<Country[]>(`${this.baseUrl}/countries`);
  }

  deleteCountry(id: number): Observable<unknown> {
    return this.http.delete(`${this.baseUrl}/country/${id}`)
  }

  addCountry(country: Country): Observable<unknown> {
    return this.http.post(`${this.baseUrl}/countries`, country);
  }

  getCountry(id: number): Observable<Country> {
    return this.http.get<Country>(`${this.baseUrl}/country/${id}`);
  }

  updateCountry(country: Country): Observable<unknown> {
    return this.http.put(`${this.baseUrl}/countries`, country);
  }



  // MOCK API JSONPLACEHOLDER
  getPhotos(): Observable<any> {
    const url = 'https://jsonplaceholder.typicode.com/photos';
    return this.http.get(url);
  }
}
