import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators'

import { TokenService } from '../token.service';

@Component({
  selector: 'sda-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public tokenService: TokenService) { }

  ngOnInit(): void {
  }


  generateToken(): void {
    this.tokenService.token = 5;
    const number = interval(1000);
    const takeNumbers = number.pipe(take(10))
    takeNumbers.subscribe(x => {
      if (this.tokenService.token > 0) {
      this.tokenService.token -= 1;
    }})
  }
}
