import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { HttpService } from '../http.service';

@Component({
  selector: 'sda-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  // form: FormGroup = new FormGroup({
  //   id: new FormControl(null),
  //   countryName: new FormControl(null),
  //   countryCapital: new FormControl(null),
  //   Population: new FormControl(null),
  //   flag: new FormControl(null)
  // });
  flags = FLAGS;
  isEditing = false;
  form: FormGroup = this.fb.group({
    id: null,
    countryName: [null, [Validators.required, this.forbiddenCountryName]],
    // adding validators [initial value, [sync validators], [async validators]]
    countryCapital: [null, [Validators.required], this.forbiddenCapitalName],
    population: null,
    flag: null,
  });
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private httpService: HttpService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      if (params.id) {
        this.isEditing = true;
        this.httpService.getCountry(params.id).subscribe((data: any) => {
          // do the below if your object is the same as the backend model
          this.form.patchValue(data);

          // do this if you have different models and need to map it
          // this.form.patchValue({
          //   id: data.id,
          //   countryName: data.countryName,
          //   countryCapital: data.countryCapital,
          //   population: data.population,
          //   flag: data.flag,
          // });
        });
      }
    });
  }

  // sync validation - custom made
  forbiddenCountryName(
    control: FormControl
  ): { [key: string]: boolean } | null {
    if (control?.value?.toLowerCase() === 'russia') {
      return { countryNameForbidden: true };
    } else {
      return null;
    }
  }

  // async validation - custom made
  forbiddenCapitalName(
    control: FormControl
  ): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    const promise = new Promise<ValidationErrors | null>((resolve, reject) => {
      setTimeout(() => {
        if (control.value?.toLowerCase() === 'moscow') {
          resolve({ forbiddenCapital: true });
        } else {
          resolve(null);
        }
      }, 2000);
    });
    return promise;
  }

  onSubmit(): void {
    if (this.form.valid) {
      if (this.isEditing) {
        this.httpService
          .updateCountry(this.form.getRawValue())
          .subscribe(() => {
            this.form.reset();
            this.router.navigate(['/table']);
          });
      } else {
        this.httpService.addCountry(this.form.getRawValue()).subscribe(() => {
          this.router.navigate(['/table']);
        });
      }
    } else {
      alert("don't hack my form");
    }
  }
}

const FLAGS = [
  { name: 'Poland', value: '../../assets/poland.png' },
  { name: 'Germany', value: '../../assets/germany.png' },
  { name: 'Bhutan', value: '../../assets/bhutanFlag.png' },
  { name: 'Nepal', value: '../../assets/nepalFlag.png' },
  { name: 'China', value: '../../assets/chinaFlag.png' },
  { name: 'India', value: '../../assets/indiaFlag.png' },
];
